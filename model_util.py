import torch
import tqdm
from pruner_util import *

def train_one_epoch(model, train_loader, criterion, optimizer, device, mask, layer_type=torch.nn.Conv2d):
    model.train()
    running_loss = 0.0
    #  remove the tqdm once done

    for inputs, targets in tqdm.tqdm(train_loader, total=len(train_loader), desc='Training', leave=False):
        inputs, targets = inputs.to(device), targets.to(device)
        optimizer.zero_grad()
        outputs = model(inputs)
        loss = criterion(outputs, targets)
        loss.backward()
        optimizer.step()
        running_loss += loss.item() * inputs.size(0)
    epoch_loss = running_loss / len(train_loader.dataset)
    return epoch_loss


def test_model(model, test_loader, device, mask, layer_type=torch.nn.Conv2d):
    # Evaluate the pruned model on the test set
    model.eval()
    with torch.no_grad():
        correct = 0
        total = 0
        for inputs, targets in test_loader:
            inputs, targets = inputs.to(device), targets.to(device)
            outputs = model(inputs)
            _, predicted = torch.max(outputs, 1)
            total += targets.size(0)
            correct += (predicted == targets).sum().item()
        test_acc = 100 * correct / total
    return test_acc
     