import torch
import torch.nn as nn
from torch.utils.data import DataLoader
from torchvision.datasets import MNIST
from torchvision.transforms import ToTensor

def getModel():
    return   nn.Sequential(
    nn.Conv2d(1, 32, kernel_size=3, padding=1),
    nn.ReLU(),
    nn.Conv2d(32, 64, kernel_size=3, padding=1),
    nn.ReLU(),
    nn.MaxPool2d(2),
    nn.Flatten(),
    nn.Linear(64*14*14, 128),
    nn.ReLU(),
    nn.Linear(128, 10)
    )

def getData(batch_size):
    # Download the MNIST DATASET and create a PyTorch dataloader
    train_dataset = MNIST(root="data/", train=True, transform=ToTensor(), download=True)
    #  keep only half of the dataset
    train_dataset = torch.utils.data.Subset(train_dataset, range(0, len(train_dataset), 2))

    train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)

    test_dataset = MNIST(root="data/", train=False, transform=ToTensor(), download=True)
    #  keep only half of the dataset
    test_dataset = torch.utils.data.Subset(test_dataset, range(0, len(test_dataset), 2))
    test_loader = DataLoader(test_dataset, batch_size=batch_size, shuffle=False)

    return train_loader, test_loader