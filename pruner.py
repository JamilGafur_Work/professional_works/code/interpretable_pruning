import torch
import numpy as np
import tqdm
import pdb
from model_util import *
from pruner_util import *
import tqdm
def iterative_magnitude_prune(model, criterion, optimizer, train_loader, test_loader, k_steps, device, layer_type=torch.nn.Conv2d, percent_to_prune=0.1, steps_to_prune=10, n_steps=10):

    data = {}
    model.to(device)
    model.train()
    
    initalWeights = get_trainable_weights(model, layer_type)
    mask = np.ones_like(initalWeights)
    # train for k_steps
    for _ in tqdm.tqdm(range(1, k_steps+1), desc="inital training"):
        train_one_epoch(model, train_loader, criterion, optimizer, device, mask, layer_type)
    mask = update_mask(model, mask, 0.0, layer_type)
    testAcc = test_model(model, test_loader, device, mask, layer_type)
    print("test accuracy: ", testAcc, "%\n\n")

    data[0] = {"mask": mask, "weights": model.state_dict()}

    # will reset the model to K_w
    k_weights = model.state_dict()

    prune_percent_per_epoch = percent_to_prune / steps_to_prune
    prune_epoch = prune_percent_per_epoch
    # we will go through steps_to_prune cycles
    for _ in tqdm.tqdm(range(1, steps_to_prune+1), desc="pruning"):
        # training for n_steps
        for _ in range(1, n_steps+1):
            train_one_epoch(model, train_loader, criterion, optimizer, device, mask)
        mask = update_mask(model, mask, prune_epoch, layer_type)
        # test the model
        testAcc = test_model(model, test_loader, device, mask, layer_type)
        print("test accuracy: ", testAcc, "%\n\n")
        # update the mask
        

        data[np.round(prune_epoch,3)] = {"mask": mask, "weights": model.state_dict()}
        assert checkassert(model, prune_epoch, mask, layer_type)
        # print the model weights that were pruned
        model.load_state_dict(k_weights)
        # we dont want to prune the zeros
        prune_epoch += prune_percent_per_epoch

    return data


def checkassert(model, prune_epoch, mask, layer_type):
    assert np.round(sum(mask)/len(mask), 3) == np.round(1-prune_epoch, 3)
    index = 0
    for module in model.modules():
        if isinstance(module, layer_type):
            weights = module.weight.data.cpu().numpy().flatten()
            mask_weights = mask[index:index+len(weights)]
            # assert that the weights are pruned
            # get index of where weights are zero
            weight_zero_index = np.where(weights == 0)[0]
            # get index of where mask_weights are zero
            mask_weights_zero_index = np.where(mask_weights == 0)[0]
            # assert that the two are the same
            
            assert np.array_equal(weight_zero_index, mask_weights_zero_index)

            index += len(weights)



    return True


   