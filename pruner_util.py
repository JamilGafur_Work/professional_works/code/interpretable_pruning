import torch
import numpy as np

def get_trainable_weights(model, layer_type):
    """
    Returns a numpy array of the trainable weights of the model
    """
    weights = []
    for module in model.modules():
        if isinstance(module, layer_type):
            weights.append(module.weight.data.cpu().numpy().flatten())

    return np.concatenate(weights)

def apply_mask(model, mask, layer_type):
    index = 0
    for module in model.modules():
        if isinstance(module, layer_type):        
            shape = module.weight.shape
            weight = module.weight.data.cpu().numpy().flatten()
            weight *= mask[index:index+len(weight)]
            module.weight.data = torch.from_numpy(weight).to(module.weight.device)
            # reshape the weight back to the original shape
            module.weight.data = module.weight.data.reshape(shape)
            index += len(weight)

def update_mask(model, mask, prune_percent, layer_type):
    """
    Updates the mask based on the prune_percent
    """
    currentWeights = get_trainable_weights(model, layer_type)
    assert len(currentWeights) == len(mask)

    # get the number of weights to prune
    threshold = np.percentile(np.abs(currentWeights), prune_percent*100)
    # get the indices of the weights to prune
    indices = np.where(np.abs(currentWeights) < threshold)[0]
    # update the mask
    mask[indices] = 0
    print("Updating mask with prune percent: ", prune_percent)
    print("sum of mask: ", np.sum(mask))

    apply_mask(model, mask, layer_type)
    return mask