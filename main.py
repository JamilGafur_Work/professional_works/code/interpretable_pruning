from util import *
from pruner import *



def main():
    batch_size = 1024
    # Create a model 
    model = getModel()
    initalWeights = model.state_dict()

    # get data
    train_loader, test_loader = getData(batch_size)

    # Define the loss function and optimizer
    criterion = torch.nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=0.001)

    # Train the model
    data = iterative_magnitude_prune(model, criterion, optimizer, train_loader, test_loader, 5, 'cuda', torch.nn.Conv2d, 0.9, 3, 2)
    import pdb; pdb.set_trace()

if __name__ == '__main__':
    main()